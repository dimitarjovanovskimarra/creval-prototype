﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Appraiser.Services
{
    public interface IPythonService
    {
        public bool RunPythonScript(int appId);
    }
}
